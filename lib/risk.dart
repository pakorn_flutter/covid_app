import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  var riskvalues = [false, false, false, false, false, false, false];
  var risks = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Question'),
      ),
      body: ListView(
        children: [
          CheckboxListTile(
              value: riskvalues[0],
              title: Text(risks[0]),
              onChanged: (newValue) {
                setState(() {
                  riskvalues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskvalues[1],
              title: Text(risks[1]),
              onChanged: (newValue) {
                setState(() {
                  riskvalues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskvalues[2],
              title: Text(risks[2]),
              onChanged: (newValue) {
                setState(() {
                  riskvalues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskvalues[3],
              title: Text(risks[3]),
              onChanged: (newValue) {
                setState(() {
                  riskvalues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskvalues[4],
              title: Text(risks[4]),
              onChanged: (newValue) {
                setState(() {
                  riskvalues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskvalues[5],
              title: Text(risks[5]),
              onChanged: (newValue) {
                setState(() {
                  riskvalues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskvalues[6],
              title: Text(risks[6]),
              onChanged: (newValue) {
                setState(() {
                  riskvalues[6] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saverisk();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_values') ??
        "[false,false,false,false,false,false,false]";
    print(strRiskValue.substring(1, strRiskValue.length - 1));
    var arrStrRiskValues =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrRiskValues.length; i++) {
        riskvalues[i] = (arrStrRiskValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saverisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_values', riskvalues.toString());
  }
}
